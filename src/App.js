import React from "react";
import axios from 'axios';
import "./App.css";
import Swal from 'sweetalert2'
import ModalForm from './Components/ModalForm';
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Table,
  Button,
  Container,
} from "reactstrap";

const URL = "http://localhost:4000/api/users";

class App extends React.Component {

  state = {
    id:'',
    data: [],
    modalTitle:"",
    modalActive:false,
    modal:"create",
    errorForm:"",
    form: {
      name: "",
      lastname: "",
      email: "",
      phone:""
    },
  };

  componentDidMount() {
    this.getUsers()
  }

  alertNotification = () => {
    Swal.fire({
      title: 'Procesado con exito!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok'
    }).then((result) => {
      if (result.isConfirmed) {
        this.getUsers()
      }
    })
  }

  errorNotification = () => {
    Swal.fire({
      title: 'Ocurrio un error!',
      text: this.state.errorForm,
      icon: 'error',
      showCancelButton: false,
      confirmButtonText: 'Ok'
    }).then((result) => {
      if (result.isConfirmed) {
        this.setState({ errorForm:"" })
      }
    })
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  getUsers = () => {
    axios.get(URL)
      .then(res => {
        const response = res.data;
        this.setState({ data:response.data });
      }).catch((error) => {
        if (error.response) {
          console.log(error.response)
          if (error.response.status === 404) {
            this.setState({ data:[] });
          }
        } else {
          this.errorNotification()
        }
      })
  }

  createUser = () => {
    let _this = this;
    let data = {...this.state.form};
    axios.post(URL,data)
      .then(res => {
        this.alertNotification()
        this.setState({ modalActive: false});
      }).catch((error) => {
        if (error.response) {
          this.setState({ errorForm:error.response.request.responseText })
        }
        _this.errorNotification()
      })
  }

  editUser = () => {
    let _this = this;
    let data = {...this.state.form};
    delete data['email']
    axios.put(`${URL}/${this.state.id}`,data)
      .then(res => {
        this.alertNotification()
        this.setState({ modalActive: false });
      }).catch(function(error) {
        if (error.response) {
          this.setState({ errorForm:error.response.request.responseText })
        } 
        _this.errorNotification()
    })
  }

  deleteUser = (id) => {
    let _this = this;
    Swal.fire({
      title: 'Estas seguro?',
      text: "No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, quiero eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'El usuario fue eliminado',
          'success'
        )

        axios.delete(`${URL}/${id}`)
        .then(res => {
          this.getUsers();
          this.setState({ modalActive: false });
        }).catch(function(error) {
          this.setState({ errorForm:error.response.request.responseText })
          _this.errorNotification() 
        })  
      }
    })
  }

  showModalUpdate = (data) => {
    let form = {...this.state.form};
    delete form['email']
    this.setState({
      modal:"update",
      form: data.form,
      id: data.id,
      modalTitle:"Actualizar usuario",
      modalActive: true,
    });
  };
  
  showModalCreate = () => {
    this.setState({
      form: {
        name: "",
        lastname: "",
        email: "",
        phone:""
      }
    });
    this.setState({
      modal:"create",
      modalTitle:"Crear usuario",
      modalActive: true,
    });
  };  

  closeModal = () => {
    this.setState({ modalActive: false });
  };

  render() {
    
    return (
      <>
        <br />
        <Container>
          <Button color="success" onClick={()=>this.showModalCreate()}>Crear</Button>
          <br />
          <br />
          <Table className="table_user">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Telefono</th>
                <th>Email</th>
              </tr>
            </thead>

            <tbody>
              {this.state.data.map((dato) => (
                <tr key={dato._id}>
                  <td>{dato._id}</td>
                  <td>{dato.name}</td>
                  <td>{dato.lastname}</td>
                  <td>{dato.phone}</td>
                  <td>{dato.email}</td>
                  <td>
                    <Button color="primary" onClick={() => this.showModalUpdate({form:dato,id:dato._id})}>
                      Editar
                    </Button>{" "}
                    <Button color="danger" onClick={()=> this.deleteUser(dato._id)}>Eliminar</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
        
        <ModalForm 
          title = {this.state.modalTitle}
          open = {this.state.modalActive}
          onChange = {this.handleChange}
          form = {this.state.form}
          fnUser = {this.state.modal === "create" ? this.createUser: this.editUser}
          fnCancel = {this.closeModal}
          readOnly = {this.state.modal === "update" ? true: false}
        />

      </>
    );
  }
}
export default App;
