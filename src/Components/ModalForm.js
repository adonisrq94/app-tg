import React from 'react';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    FormGroup,
    ModalFooter,
  } from "reactstrap";

class ModalForm extends React.Component {
 
  render() {
    return (
        <Modal isOpen={this.props.open}>
        <ModalHeader>
         <div><h3>{this.props.title}</h3></div>
        </ModalHeader>

        <ModalBody>
          
        <FormGroup>
            <label> Nombre: </label>
            <input className="form-control" name="name" type="text" 
                   onChange={this.props.onChange} value={this.props.form.name} 
            />
          </FormGroup>

          <FormGroup>
            <label> Apellido: </label>
            <input className="form-control" name="lastname" type="text" 
                   onChange={this.props.onChange} value={this.props.form.lastname} 
            />
          </FormGroup>

          <FormGroup>
            <label> phone: </label>
            <input className="form-control" name="phone" type="number" 
                   onChange={this.props.onChange} value={this.props.form.phone} 
            />
          </FormGroup>

          <FormGroup>
            <label> Email: </label>
            <input className="form-control" name="email" type="email"  readOnly={this.props.readOnly} 
                   onChange={this.props.onChange} value={this.props.form.email} 
            />
          </FormGroup>

        </ModalBody>

        <ModalFooter>
          <Button
            color="primary"
            onClick={() => this.props.fnUser() }
          >
            OK
          </Button>
          <Button
            className="btn btn-danger"
            onClick={() => this.props.fnCancel()}
          >
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ModalForm;
